/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package neveraprueba;

import javax.swing.JOptionPane;

/**
 *
 * @author Christian Acevedo
 * @version 1.2 05 de Junio de 2014
 */

public class Probando {
    
    String nombre;
    String cantidad;
    

    /**
     * 
     * Metodo que comprueba que no se puedan meter mas de 10 letras en el nombre
     * 
     * @param nombre
     * @return datosNevera, nos devolverá true si los datos son correctos y false si no.
     */
    public boolean datos(String nombre){
        /**
         * //La variable a comprobar la inicializamos a false
         */
        boolean datosNevera = false;
        
        System.out.println("introduzca el nombre del articulo (10 letras maximo)");
        /**
         * //Si el numero de letras es 11 o mas, nos devolverá el siguiente mensaje
         */
        if (nombre.length()>=11){
            System.out.println("Has escrito mas letras de la cuenta");
            datosNevera=false;
        }
        /**
        //Si el campo lo dejamos en blanco, nos devolverá el siguiente mensaje
        */
        if(nombre.length()==0){
            System.out.println("No has escrito nada");
            datosNevera=false;
        }
        /**
        //Si los datos introducidos son correctos, nos devolverá lo que hayamos introducido
        */
        else {datosNevera=true;
            System.out.println(nombre);
        }
        
        return datosNevera;
        
    }
    
    /**
     * Metodo que comprueba si se ha introducido bien la cantidad
     * @param cantidad
     * @return comprobar, nos devolverá true si hemos introducido bien los números.
     */
    public boolean numeros(String cantidad){
        boolean comprobar = false;
        System.out.println("introduzca la cantidad (solo numeros)");
        try {
            /**
             * Si la cadena se puede convertir a int, entonces devolverá true, si la dejamos en blanco o no se puede convertir, devolverá false
             */
         //Comentario necesario

		Integer.parseInt(cantidad);
		comprobar=true;
                System.out.println("Cantidad introducida correctamente");
                System.out.println("Cantidad introducida: "+cantidad);
	} catch (NumberFormatException e){
		comprobar=false;
                System.out.println("Has introducido mal la cantidad");
                System.out.println("Cantidad intorucida: "+cantidad);
	}
        return comprobar;
    }

}

