/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package neveraprueba;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Christian Acevedo
 * @version 1.2 05 de Junio de 2014
 */


public class ProbandoTest {
    
    public ProbandoTest() {
    }

    @Before
    public void setUp() {
        System.out.println("COMIENZO DE LA PRUEBA");
    }
    
    @After
    public void tearDown() {
        System.out.println("\n");
        System.out.println("FINAL DE LA PRUEBA");
    }

    /**
     * Test of datos method, of class Probando.
     */
    @Test
    public void testDatos() {
        System.out.println("\n");
        System.out.println("--COMPROBANDO DATOS--");
        System.out.println("\n");

        Probando instance = new Probando();
        String nombre[]={"Lechuga","","Jamon Cocido Extra"};
        boolean resultado[]={true, false, true};
        for (int i=0;i<3;i++){
            boolean resultado2 = instance.datos(nombre[i]);
            assertEquals(resultado[i], resultado2);
        }
    }
    


    /**
     * Test of numeros method, of class Probando.
     */
    
    
    @Test
    public void testNumeros() {
        
        System.out.println("\n");
        System.out.println("--COMPROBANDO CANTIDADES--");
        System.out.println("\n");

        Probando instance = new Probando();
        String cantidad[]={"11","","Jamon"};
        boolean resultado[]={true, false, false};
        for (int i=0;i<3;i++){
            boolean resultado2 = instance.numeros(cantidad[i]);
            assertEquals(resultado[i], resultado2);
        }
    }
    
}
